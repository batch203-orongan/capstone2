const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userController");
const auth = require ("../auth")

console.log (userControllers);

// Routes for checking email
router.post("/checkEmail", userControllers.checkEmailExists);

// Routes for user registration
router.post("/register", userControllers.registerUser);

// Route for user Authentication
router.post("/login", userControllers.loginUser);

// Route for details of a user
router.get("/details", auth.verify, userControllers.getProfile);

// Route for adding an order
router.post("/order", auth.verify, userControllers.createOrder);

// Routes for updating a user
router.put("/:userId", auth.verify, userControllers.updateUser);

// Route for viewing all orders
router.get("/myOrders", auth.verify, userControllers.getMyOrders);

// Route for viewing all orders (admin only)
router.get("/allOrders", auth.verify, userControllers.getAllOrders);

module.exports = router;