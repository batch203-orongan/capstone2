const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productController");
const auth = require("../auth");

console.log(productControllers);

// Route for creating product
router.post("/",auth.verify, productControllers.addProduct);

// Route for viewing all products (admin only)
router.get("/all", auth.verify, productControllers.getAllProducts);

// Route for viewing all available product (all users)
router.get("/", productControllers.getAllAvailableProducts);

// Routes for viewing a single product
router.get("/:productId",productControllers.getSingleProduct);

// Routes for updating a product
router.put("/:productId", auth.verify, productControllers.updateProduct);

// Routes for archiving a product
router.patch("/archive/:productId", auth.verify, productControllers.archiveProduct);

module.exports = router;