const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	numberOfStocks:{
		type: Number,
		required: [true, "numberOfStocks is required"]
	},
	isAvailable:{
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [{
		userId: {
			type: String,
			required: true
		},
		userEmail: {
			type: String
		},
		quantity: {
			type: Number,
			required: true
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}]
});

module.exports = mongoose.model("Product", productSchema)