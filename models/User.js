const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNumber: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	orders: [{
		totalAmount: {
			type: Number
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		products: [{
			productId: {
				type: String,
				required: true
			},
			productName: {
				type: String,
			},
			quantity: {
				type: Number,
				required: true
			},
			price: {
				type: Number
			}
		}]
	}]
});

module.exports = mongoose.model("User", userSchema)