const jwt = require("jsonwebtoken");
const secret = "NeverGiveUp";

// Token Creation

module.exports.createAccessToken = (user) => {

	console.log(user);

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});

};

// Middleware function
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if(token !== undefined){

		token = token.slice(7, token.length);

		console.log(token);

		return jwt.verify(token, secret, (err, data) =>{
			// if JWT is not valid
			if(err){
				return res.send({auth: "Invalid token"})
			}
			// if JWT is valid
			else{
				next()
			}
		})
	}
	else{
		res.send({message: "Auth failed! No token provided!"})
	}
}

// Token Decryption

module.exports.decode = (token) => {
	if(token !== undefined){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) =>{
			if(err){
				return null;
			}
			else{
				return jwt.decode(token, {complete: true}).payload
			}
		})
	}
	else{
		return null
	}
}