const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt =require("bcrypt");
const auth = require("../auth");

// Check if email exist

module.exports.checkEmailExists = (req, res) => {
	return User.find({email: req.body.email}).then(result => {
		if(result.length > 0){
			return res.send(true);
		}
		else{
			return res.send(false);
		}
	})
	.catch(error => res.send(error));
}

// User Registration

module.exports.registerUser = (req, res) => {

	let newUser = new User({
		firstName: 		req.body.firstName,
		lastName: 		req.body.lastName,
		email: 			req.body.email,
		password: 		bcrypt.hashSync (req.body.password, 10),
		mobileNumber: 	req.body.mobileNumber
	})

	console.log(newUser);

	return newUser.save()
	.then(user => {
		console.log(user);
		
		res.send(true);
	})
	.catch(error => {
		console.log(error);
		
		res.send(false);
	})
}

// User Authentication

module.exports.loginUser = (req, res) => {
	return User.findOne({email: req.body.email})
	.then(result => {
		// User does not exist
		if(result == null){
			return res.send ({message: "No User Found!"});
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(result)})
			}
			else{
				return res.send({message: "Incorrect password!"});
			}
		}
	})
}

// User Details

module.exports.getProfile = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	console.log(userData);

	return User.findById(userData.id).then(result =>{
		result.password = "***"
		return res.send(result)
	})	
}

// Add Order
module.exports.createOrder = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(!userData.isAdmin){

		for( let i=0; i <= req.body.products.length ; i++ ){

			let product = await Product.findById(req.body.products[i].productId);
			let quantity = await Product.findById(req.body.products[i].productId).then(result => req.body.products[i].quantity);

			let data = {
				userId: 		userData.id,
				email: 			userData.email,
				productId: 		req.body.products[i].productId,
				productName: 	product.name,
				quantity: 		quantity,
				price: 			product.price * quantity
			};

			console.log(data);

			// check valid quantity input
			if(  quantity > 0){

				// check available stocks 

				if( product.numberOfStocks !== 0 
					&& product.numberOfStocks >= quantity){

					let isUserUpdated = await User.findById(data.userId)
					.then(user => {

						let products = {
							productId: 		data.productId,
							productName: 	data.productName,
							quantity: 		data.quantity,
							price: 			data.price
						};

						
						user.orders.push({
							products, 
							totalAmount: req.body.totalAmount
						});

						return user.save()
						.then(result => {
							console.log(result);
							return true;
						})
						.catch(error =>{
							console.log(error);
							return false;
						})
					})

					console.log(isUserUpdated);

					let isProductUpdated = await Product.findById(data.productId).then(product =>{
						product.orders.push({
							userId: 	data.userId,
							email: 		data.email,
							quantity: 	data.quantity
						})

						product.numberOfStocks -= data.quantity;

						return product.save()
						.then(result => {
							console.log(result);
							return true;
						})
						.catch(error =>{
							console.log(error);
							return false;
						})

					})
					

					console.log(isProductUpdated);

					(isUserUpdated && isProductUpdated) ? res.send(true) : res.send(false)

				}
				else {
					return res.status(400).send(`${data.productName} is currently out of stock!
						We have ${product.numberOfStocks} stocks available for this item.`)
				}
			}
			else{
				// input quantity is 0
				return res.status(401).send("Please input valid quantity. Quantity should be greater than zero.");
			}

		}
		
	}
	else{
		return res.status(402).send("You cannot request an order as an administrator!");
	}
	
}


// Update a User

module.exports.updateUser = (req, res) =>{

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin){
		let updateUser = {
			isAdmin: 	req.body.isAdmin
		}

		return User.findByIdAndUpdate(req.params.userId, updateUser, {new: true})
		.then(result =>{
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		});
	}
	else{
		return res.status(403).send("You don't have access to this page!");
	}
}

// Retrieve all authenticated orders

module.exports.getMyOrders = (req, res) =>{
	
	const userData = auth.decode(req.headers.authorization);

	return User.findById(userData.id).then(result => res.send(result.orders));
}


// Retrieve all orders (admin only)

module.exports.getAllOrders = (req, res) =>{
	
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		return User.find({}).then(result => res.send(result));
	}
	else{
		return res.status(404).send("You don't have access to this page!");
	}
}



