const Product = require("../models/Product");
const User = require("../models/User");
const auth = require("../auth")

// create new product

module.exports.addProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let newProduct = new Product({
		name : 			req.body.name,
		description : 	req.body.description,
		price : 		req.body.price,
		numberOfStocks: req.body.numberOfStocks
	});

	if(userData.isAdmin){

		return newProduct.save()
		.then(product => {
			console.log(product);
			res.send(true)
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		});
	}
	else {
		return res.status(401).send("You don't have access to this page!");
	};

};

// Retrieve all products (admin only)

module.exports.getAllProducts = (req, res) =>{
	
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		return Product.find({}).then(result => res.send(result));
	}
	else{
		return res.status(401).send("You don't have access to this page!");
	}
}

// Retrieve all available products (all users)

module.exports.getAllAvailableProducts = (req, res) =>{
	return Product.find({isAvailable: true}).then(result => res.send(result));
}

// Retrieving a single product

module.exports.getSingleProduct = (req, res) =>{
	console.log(req.params.productId);

	return Product.findById(req.params.productId).then(result => res.send(result));
}

// Update a Product

module.exports.updateProduct = (req, res) =>{

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin){
		let updateProduct = {
			name: 			req.body.name,
			description: 	req.body.description,
			price: 			req.body.price,
			numberOfStocks: req.body.numberOfStocks
		}

		return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new: true})
		.then(result =>{
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		});
	}
	else{
		return res.status(401).send("You don't have access to this page!");
	}
}

// Archive a product

module.exports.archiveProduct =(req, res) =>{

	const userData = auth.decode(req.headers.authorization);

	let updateIsAvailableField = {
		isAvailable: req.body.isAvailable
	}

	if(userData.isAdmin){
		return Product.findByIdAndUpdate(req.params.productId, updateIsAvailableField, {new: true})
		.then(result => {
			console.log(result);
			res.send(true);
		})
		.catch(error =>{
			console.log(error);
			res.send(false);
		})
	}
	else{
		return res.status(401).send("You don't have access to this page!");
	}
}


